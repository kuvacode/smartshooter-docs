External API
============

*Integrating Smart Shooter with an external system*

.. note::
   This feature is only available in **PRO** versions of
   Smart Shooter.

Smart Shooter can be integrated with an external system by using the
**External API** feature. This provides two key channels of communication:

* Publisher of event information
* Server for handling command requests 

The External API uses the ZeroMQ library for managing the communication
transport layer. See `<http://zeromq.org>`_ for more information on how to use
ZeroMQ from your system. This also defines the programming model for using the
event publisher and request/reply server, as these are implementated as ZeroMQ
socket endpoints.

All messages passed over the External API are encoded in JSON format. See 
`<http://json.org>`_ for more information on how to handle this type of message
format.

.. tip::
   Sample python code for the External API is available at:
   `<https://bitbucket.org/kuvacode/smartshooter-api>`_

Listening to Events
-------------------

The event publisher will broadcast messages about important events that happen
inside Smart Shooter. This includes:

* When a new photo is taken
* When a photo changes state (downloaded/deleted/renamed etc)
* When a new camera is detected
* When a camera changes state
* When a camera property changes state

Below is an example of a listener application written in Python. This can be
run from the command line, and simply prints out each message that is received
from the Smart Shooter publisher endpoint.

.. literalinclude:: downloads/smartshooter_listen.py

:download:`downloads/smartshooter_listen.py`

For example, the text below shows the output from this after a new photo has
been taken in Smart Shooter::

   Opened listener to: tcp://127.0.0.1:54543
   Received: {
     "msg_type": "Event",
     "msg_id": "PhotoUpdatedMsg",
     "msg_ref_num": 71,
     "PhotoSelection": "Single",
     "PhotoKey": "f4a5b1f0-f409-4019-b59c-84966ece0a53",
     "PhotoLocation": "Camera",
     "PhotoUUID": "f4a5b1f0-f409-4019-b59c-84966ece0a53",
     "PhotoOriginalName": "DSC_0001.jpg",
     "PhotoComputedName": "SSP_1143.jpg",
     "PhotoDateCaptured": "20150323 21:54:14.878",
     "PhotoFormat": "Unknown",
     "PhotoWidth": 0,
     "PhotoHeight": 0,
     "PhotoFilesize": 763471,
     "PhotoIsImage": true,
     "PhotoIsScanned": false,
     "GridSequenceNum": 1143,
     "GridBatchNum": 1057,
     "CameraKey": "Nikon Corporation|D7000|6181433"
   }

Sending Requests
----------------

The request/reply server inside Smart Shooter can handle requests to do various
actions such as:

* Connect/Disconnect camera
* Take photo
* Download/rename/delete photo
* Change camera property
* Auto focus camera
* Change sequence/batch number

.. tip::
    For further documentation see the public git repo:
    `<https://bitbucket.org/kuvacode/smartshooter-api>`_

    And in particular the API documenation stored here:
    `<https://bitbucket.org/kuvacode/smartshooter-api/src/master/external_api.rst>`_

Configuring the External API
----------------------------

By default the External API is disabled. To enable it, go to the
**Network/API** tab in the **Options** window and enable the checkbox, as
shown below. This also allows customisation of the ZeroMQ endpoints that are
used for the event publisher and request/reply server, so that it can be opened
up for access from other computers on a network.

.. figure:: images/external_API.jpg
