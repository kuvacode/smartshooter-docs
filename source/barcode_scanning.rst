Barcode Scanning
================

*How to scan your photos for barcode information*

.. note::
   This feature is only available in **PRO** versions of Smart Shooter.

Smart Shooter has the ability to scan a photo and detect if it contains a
barcode or QR code, and extract the text information from it. This text can
then be used as part of the filename when saving the photo.

When a barcode is successfully scanned, it is saved and shown in the
**Filename Options** window.

The next time Smart Shooter has to compute a photo filename, it will use this
barcode text to replace the ``[Z]`` parts of the filename expression. For
more information on how the filename expression works, see :ref:`Name Policy`.


.. figure:: images/mug_qr_code.jpg

The screenshot above shows a demonstration. The QR code in the photo contains
the text ``mug``. After the photo was taken, it was
automatically downloaded and then scanned by Smart Shooter. The text was
extracted and then put in the **Barcode** text box in the **Filename Options**
window. The filename used when saving the photo contains the same text.

Automatic Scanning
------------------

You can configure Smart Shooter to automatically scan any new photos for a
barcode. If you are using multiple cameras, this automatic scanning can be
restricted to only scan photos taken by certain cameras. The screenshot below
shows these in the **Options** window.

Whenever a barcode is automatically scanned, it will affect the filename of
that particular photo, and then all subsequent photos until another barcode is
scanned.

Automatic scanning is also performed for liveview images, so a barcode can be
extracted directly from the liveview stream even before a photo is taken.

.. figure:: images/barcode_tab.jpg

Manually Scanning
-----------------

.. |barcode button| image:: images/barcode_button.png

The barcode scanning can be triggered manually at any time by pressing the
|barcode button| button at the
top of the **Capture** window. Smart Shooter will try to scan a barcode from
whichever image is visible at that time, and this can include a previously
taken photo or a liveview image.

Supported Barcode Types
-----------------------

The following barcode symbologies are supported: 

* QR Code
* EAN-13/UPC-A
* UPC-E
* EAN-8
* Code 128
* Code 39
* Interleaved 2 of 5

Smart Shooter uses the ZBar open source library for detecting barcodes. Their
website is: `<http://zbar.sourceforge.net>`_

Using an External Barcode Scanner
---------------------------------

Its also possible to use an external barcode scanner with Smart Shooter. As
long as this is also connected to the same computer, and can be configured to
enter text in a UI text box, you can have it fill in the **Barcode** text box
of the **Filename Options** window.
