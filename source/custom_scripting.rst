Custom Scripting
================

Automatic Control via Custom Scripting
--------------------------------------

Smart Shooter can be driven from a script, giving you the ability to change camera settings and control when photos are taken. This allows you to take a series of different photos without manual operation of the camera or PC. You can find documentation on creating Custom scripts `here <http://ssdocs.tethertools.com/user_guide.html#d-scripts>`_.