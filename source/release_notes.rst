Release Notes
=============

Version 4.25
------------

6th August 2022
^^^^^^^^^^^^^^^

* Add support for Sony a7 IV
* Add support for Sony ZV-E10
* Add support for Canon R7
* Add support for Canon R10
* Add support for Liveview zoom/position on Sony cameras
* Add support for direct shutter button control on Sony cameras
* Fix crash caused by bug with texture loading in UI layout logic
* Fix issue with shutter button control during continuous highspeed shooting
* Fix issue with Sony triggering during MF focus mode
* Fix incorrect loupe size when loading photo thumbnails
* Disable Lightroom plugin install button for mac app store version
* Remove barcode metadata checkbox from options UI

Version 4.24
------------

28th March 2022
^^^^^^^^^^^^^^^

* Fix issue with photo quality setting being ignored on Sony cameras

Version 4.23
------------

27th October 2021
^^^^^^^^^^^^^^^^^

* Adjust Sony live view focus sensitivity
* Ensure Nikon cameras are disconnected gracefully
* Ensure Recent Photos list auto-scrolls to end when new photo is added
* Fix clickable UI area for camera live view popup menu
* Fix typo in tooltip for focus step buttons
* Improve log message for failed license activation request

Version 4.22
------------

7th May 2021
^^^^^^^^^^^^

* Add support for Sony a1
* Add support for Canon R5
* Add support for Canon R6
* Add support for Canon 90D
* Add support for Canon 850D / Rebel T8i / Kiss X10i
* Add support for full control of camera settings on Sony a7R4, a7C, a1
* Add support for Pixel Shift mode on recent Sony cameras
* Add display option to enable/disable filmstrip and tool panels
* Add photo filename validation feature
* Adjust Sony live view focus sensitivity
* Allow Colour Temp setting to be part of stored camera preset
* Fix Storage mode setting for recent Sony cameras
* Fix intermittent failure of live view on Sony cameras
* Fix video download on Canon EOS R
* Fix quick loupe for live view on Sony cameras
* Fix issue with background image being hidden until first photo is taken
* Ensure Lightroom retains focus when plugin is launched from within Lightroom
* Ensure empty files are removed if video download fails
* Ensure live view is marked as not-supported on the Sony a6000

Version 4.20
------------

29th January 2021
^^^^^^^^^^^^^^^^^

* Add support for Sony a7C and a7S III
* Add full support for Canon CR3 file format
* Add support for live view focus with Sony Alpha cameras
* Add support for controlling power zoom on some Sony Alpha cameras
* Fix live view on Sony a7C and similar models
* Fix issue with dropped replies for External API requests

Version 4.19
------------

11th November 2020
^^^^^^^^^^^^^^^^^^

* Improve barcode scanning accuracy for small 1D type barcodes
* Fix broken video file downloads on Canon cameras
* Add support for setting custom White Balance colour temperature on Nikon and Sony cameras
* Add link to Lightroom plugin install instructions
* Ensure invalid External API requests are correctly rejected
* Update message in trial ended notification window

Version 4.18
------------

5th October 2020
^^^^^^^^^^^^^^^^

* Add support for Lightroom tethering plugin
* Add support for Nikon D6
* Add support for Nikon D780
* Add support for Nikon Z 5
* Add support for Nikon Z 50
* Add support for Canon RP
* Add support for Canon M6 Mark II
* Add system tray icon to mac app
* Add command line option to start minimised to system tray
* Improve accuracy of barcode scanning
* Prepare support for macOS Big Sur and Apple silicon
* Do not exit app when minimised to system tray and main window is closed
* Fix incorrect msg_seq_num on External API replies

Version 4.17
------------

1st July 2020
^^^^^^^^^^^^^

* Fix issue with flickering liveview on Sony cameras
* Fix use of Windows network shares in Photo Download Directory
* Fix deadlock issue after barcode scan triggers photo renaming
* Add help menu item for submitting feedback email
* Show full product SKU in License window
* Decrease tolerance for over/under exposure warning

Version 4.16
------------

24th May 2020
^^^^^^^^^^^^^

* Add PRO upgrade link to Help menu and About window
* Add button to License window for rechecking license
* Add license type and order ID to License window
* Make PRO features visible but disable for standard version
* Fix issue with UI focus when trying to change/edit camera from camera table

Version 4.15
------------

22nd April 2020
^^^^^^^^^^^^^^^

* Improve reliability of Sony capture when AF is enabled
* Improve latency of Sony AF capture
* Improve highlight graphics for filmstrip photos
* Fix Sony auto focus functionality
* Fix camera status when Sony MF capture fails
* Fix issues with Sony photo download during highspeed continuous shooting
* Fix crash when value for script parameter contains empty text
* Detect Sony storage mode setting from camera
* Add option to enable/disable Capture display
* Add system tray icon to windows app
* Add option to start app minimised to system tray
* Add highlight for current camera in Cameras display panel
* Add StorageID, PersistentID, and OriginalPath to External API photo info
* Add GetCamera request message to External API
* Add option to persist photo info after app restart
* Support restore of photo info from previous session
* Allow app to be activated/deactivate via External API
* Allow Camera Controls current camera to be selected from Cameras display panel
* Ensure any outstanding Sony autofocus requests are cancelled before disconnect
* Restore camera selection mode after app restart

Version 4.14
------------

19th February 2020
^^^^^^^^^^^^^^^^^^

* Improve image quality of previews and thumbnails in photo display window
* Add option to disable Live View focus with mouse wheel
* Add support for bugsplat crash reporting on mac
* Add error code descriptions to cryptlex license window UI
* Ensure correct texture level is chosen in photo preview window
* Ensure Quick Loupe uses fullsize photo
* Ensure control characters (ie. newline) are stipped from photo filename
* Ensure PhotoFilename field is sent to external API event stream
* Fix issue with Sony camera properties getting out of sync
* Fix issue with Sony capture requests returning early failure
* Fix issue with histogram when two display windows are used
* Fix error when running Bulb Timelapse script
* Fix crash when loading background image

Version 4.13
------------

16th January 2020
^^^^^^^^^^^^^^^^^

* Fix issue with photo detection and download on Sony a7R4
* Fix issue with sticky UI controls on mac
* Fix crash when using multiple display windows on mac
* Add mappings for more Sony a7R4 camera settings
* Show trial ID in trial/license window
* Ensure log file is updated on regular interval

Version 4.12
------------

31st December 2019
^^^^^^^^^^^^^^^^^^

* Fix issue with update window getting stuck in background on mac
* Fix issue with Live View FPS controls
* Fix issue with photo names contained absolute files paths
* Fix External API fields for CameraNumDownloadsComplete and CameraNumDownloadsFailed
* Add option for setting default Live View FPS
* Add rate limiter to photo viewer to avoid backlog of loading/decoding operations
* Stop sendling LiveviewUpdated message to External API

Version 4.11
------------

15th November 2019
^^^^^^^^^^^^^^^^^^

* Add support for photo quality settings on Sony cameras
* Add option to hide on-card photos from filmstrip
* Add new OptionsUpdated External API message
* Add UI to allow formatting memory cards in specific cameras
* Change 'Delete All' button to only delete known photos and not download directory
* Improve UI for hiding/unhiding photos
* Fix support for RAW+JPEG mode on Sony cameras
* Ensure large log file does not persist once app is restarted
* Ensure app update checks do not happen while activation window is still visible
* Ensure OptionsInfo and DownloadPath are added to External API synchronistion reply
* Ignore folders created by camera on memory card
* Ignore duplicate photos when shooting to dual memory card slots
* Avoid possible app hang when handling USB communication errors on Windows

Version 4.10
------------

28th October 2019
^^^^^^^^^^^^^^^^^

* Switch to new Cryptlex licensing system
* Add option to disable automatic rotation using photo EXIF
* Add option to apply photo rotation using camera metadata
* Add option to auto fill Session Name or Unique Tag when clipboard text changes
* Add checkbox for pausing output text in log window
* Add single 'Manage License' item to help menu
* Fix USB host controller detection and balancing on Windows
* Fix orientation of live view loupe when display mirroring is enabled
* Fix possible hang when closing application

Version 4.9
-----------

29th September 2019
^^^^^^^^^^^^^^^^^^^

* Add Sony support for cameras in the a6xxx range, a9, and a7 variants
* Add support for changing Sony camera properties
* Add support for live view on Sony cameras
* Add UI buttons for incrementing/decrementing camera properties
* Add script for direct shutter button control for Canon cameras
* Fix colouring of UI text for script status
* Fix photo rendering for GPUs that have limited max texture size
* Fix issue with [E] filename token when shooting RAW+JPEG
* Replace warning icons with coloured text in Camera Controls window
* Allow parallel downloads from cameras connected through same USB host controller

Version 4.8
-----------

29th August 2019
^^^^^^^^^^^^^^^^

* Add support for Canon 250D / Rebel SL3 / Kiss X10
* Add new zxing barcode scanner engine
* Add buttons for pasting clipboard into filename options text fields
* Fix issue with support for cardless operation with Canon cameras
* Fix misleading names used in PTP property log messages
* Show Card/Disk status for filmstrip thumbnail placeholder
* Replace Client Presentation Mode option with Card Preview dropdown menu
* Change behaviour of barcode text field to accept external scanner input
* Track Session Name and Session Number information with each photo
* Improve GUI table perf by disabling automatic column resizing
* Separate photo location and hidden status
* Allow photos to be unhidden

Version 4.7
-----------

25th July 2019
^^^^^^^^^^^^^^

* Fix possible crash when recovering from failed camera connection
* Fix possible crash when sending External API event messages
* Fix issue with rejected download requests when camera already has capture request pending
* Fix issue when changing UniqueTag via External API
* Fix issue with migrated camera names being overridden with default model name
* Add placeholder text for thumbnails of photos that have not yet been downloaded
* Add link to documentation website in Help menu
* Add link to documentation website in Options window
* Ensure Client Presentation Mode only downloads JPEGs when shooting RAW+JPEG
* Ensure session num UI gets updated after options update
* Ensure log file is flushed when handling bugsplat report
* Show totals for photo/camera/nodes in UI tables
* Reduce use of GPU when Capture tab is not visible
* Update UI to improve layout of photo and camera tabs
* Update UI to improve layout of Options window
* Add ability to purge files from photo download directory
* Filter out noisy Nikon ExposureIndicateStatus messages from log

Version 4.5
-----------

24th May 2019
^^^^^^^^^^^^^

* Fix live view near focus command on Nikon cameras
* Fix possibly crash in photo rendering code
* Avoid duplicate text when using [E] extension in filename expression
* Default to using 'SSP_[S]' when computed filename is empty

Version 4.4
-----------

28th April 2019
^^^^^^^^^^^^^^^

* Add persistant storage of script parameter values
* Add camera rotate button to live view UI
* Add option to disable live view status indicator
* Add option to enable/disable update check during startup
* Add icon colour highlight to indicate when display options are enabled/disabled
* Fix incorrect display of photo shutter speed info
* Fix issue with update checking
* Fix issue with maximised window state not being applied after restart

Version 4.3
-----------

12th April 2019
^^^^^^^^^^^^^^^

* Fix bug in behaviour of "Default Storage Mode" option
* Fix possible hang during shutdown if Live View is active
* Fix issue causing scripts to fail for Canon cameras when using Disk storage mode
* Fix issue causing scripts to fail for Nikon cameras when using Both storage mode
* Fix issue with Focus Stacking script that caused focus movement to be ignored
* Fix issue with old photos being shown if new photo is taken with same filename
* Fix issue with automatic barcode scanning incorrectly triggering file rename
* Allow scrolling through filmstrip using mouse wheel
* Improve performance of barcode scanning

Version 4.2
-----------

5th April 2019
^^^^^^^^^^^^^^

* Fix Camera Controls UI redraw issue on mac
* Fix crash and communication issues on mac with Nikon D500 and D850
* Fix crash on start up when Canon cameras already have pending photos to download
* Fix issue with drag movement of Live View AF region
* Allow select/view photo from filmstrip when using multi-camera display
* Filter out non-PTP USB devices (such as phones)

Version 4.1
-----------

31st March 2019
^^^^^^^^^^^^^^^

* Fix crash when camera settings get out of sync on Canon cameras
* Fix crash on mac when using Nikon D3xx range of cameras
* Fix issue with non-script files being shown in Scripts menu list

Version 4.0
------------------

28th March 2019
^^^^^^^^^^^^^^^^^^^^^^

* Added Support for New Cameras

 - Add support for Canon R
 - Add support for Nikon Z6
 - Add support for Nikon Z7
 - Add support for Nikon D3500
 - Add support for Sony a6300
 - Add support for Sony a6500
 - Add support for Sony a7R III
 - Add support for Ricoh Theta S

* Application UI

 - Add button to display toolbar for fullscreen mode
 - Add button to imgui UI to quick swap window types
 - Add photo browser gui
 - Add pulse indicate to liveview window
 - Add quick loupe for liveview
 - Add separate option to enable/disable liveview quick loupe
 - Add Smart Shooter icon to Camera Controls window
 - Add visual indicator for elapsed video recording time
 - Add visual indicator of liveview resolution and FPS
 - Allow customisation of quick loupe window size
 - Allow double click on filmstrip to start photo download
 - Allow filmstrip navigation with keyboard
 - Allow loupe window zoom using right mouse drag
 - Allow loupe zoom reset with middle mouse click
 - Allow sequence/batch num spin box to be reset by entering 0
 - Change warning icons from PNG to SVG
 - Customise About window with extra Tether Tools info
 - Disable multiply display windows on mac (not yet working)
 - Ensure UI does not display liveview image from previous session
 - Fix issue with GUI state not being restored after restart
 - Hide Barcode button from context menu for standard licenses
 - Hide multicamera selection controls for standard license
 - Highlight loupe rect when loupe window is hovered
 - Hook up liveview focus to mouse wheel
 - Improve layout of Display Options UI
 - Improve pan/zoom of loupe window
 - Improve UI layout for Camera Presets window
 - Improve UI layout of Script Controls window
 - Improve UI looks for groupbox and tooltip widgets
 - Improve UI looks of loupe window
 - Improve UI of Filename Options window
 - Improve visibility of UI cursor when editting text
 - Make "New Display" into modeless window
 - Make Recent Photos panel hidden by default
 - Remove Display Mode selection from UI
 - Remove icons from top menu bar
 - Remove unused "Restart Required" dialog from options window
 - Rename 'Display' tab to 'Capture'
 - Reword tooltips in display window button
 - Switch UI to SVG icons
 - Update UI layout of Filename Options window
 - Update with new icon and logo graphics
 - Update wording from Liveview to Live View

* Camera Fixes

 - Add checks to enable features for Nikon D3xxx cameras
 - Add handling of Canon CR2 and CRAW image format
 - Add identifiers for remaining current Canon DSLR cameras
 - Add property mappings for more Canon program modes
 - Correctly finish pending bulb tasks during camera disconnect
 - Do not show 'Unknown' when photo format is not known
 - Ensure Canon liveview status gets updated after enable/disable
 - Ensure computed filename is never empty string
 - Ensure duplicate download operations are discarded
 - Ensure photo format is populated from filename extension if unknown
 - Extract extra EXIF metadata from photo during save
 - Fix camera hotplug detection on mac
 - Fix crash on start up if camera already has download pending
 - Fix issue with auto connect not applying to multiple cameras
 - Fix issue with stale camera properties after reconnect
 - Fix possible crash when camera property goes out of sync
 - Fix possible photo save errors, if filename expression option gets set to empty text
 - Force liveview support feature for Nikon D3400 and D3500
 - Gracefully download pending canon photos after unexpected failures
 - Gracefully stop video when disconnecting from camera
 - Improve handling of Nikon continuous shooting mode
 - Improve perf of Nikon liveview update logic
 - Support check/sync clock operation on Canon cameras
 - Support extra photo metadata from EXIF

* Changes to External API

 - Add command line options to enable External API
 - Add image orientation to photo object
 - Add tracking of liveview frames with CameraLiveviewNumFrames
 - Convert External API to synchronous reply behaviour
 - Ensure CameraName is sent for first update of new camera
 - Gracefully handle exit() call from python camera scripts
 - Implement support for PhotoOrigin API field
 - Remove UTF8 BOM from json encoder output
 - Rename BulbInterval field to BulbTimer
 - Rename CameraLiveviewFocus field to CameraLiveviewFocusStep
 - Switch to python language for embedding scripting
 - Unify API using for External API and embedded python scripting

* Add Client Presentation Mode feature
* Add support for [S] session name and [I] session num filename tokens
* Add tracking of elapsed video recording time
* Add trigger index to camera metadata
* Allow control of camera operation order using trigger index
* Change custom script loading ability to PRO feature
* Delete camera presets file if all presets are deleted
* Enable high DPI mode for windows installer
* Ensure UTF8 BOM is written when saving text files
* Remove ability to send feedback from within app
* Remove JPEG camera storage mode
* Remove liveview recording feature
* Remove Nullsoft branding from windows installer
* Remove option for reseting sequence number when directory is empty
* Remove option to disable filename generation
* Remove Options Presets feature
* Remove rebranding feature
* Remove support for [N] camera index token in filename expression
* Rename executable for windows uninstaller
* Rewrite camera communication on mac to improve reliability
* Rewrite camera communication to optimise download performance
* Set smart shooter trial mode sku to PRO
* Sort execution of camera operations by trigger index
* Update app to allow running in CLI mode
* Update license texts for 3rdparty libraries
* Update mac minimum version to Mac OS X 10.12 (Sierra)
* Use simplified smartshooter logo for windows installer background

Version 3.38
------------------

8th December 2018
^^^^^^^^^^^^^^^^^^^^^^

* Fix detection of Canon 6D Mark II model name
* Fix auto focus on the Canon 6D Mark II and other Canon cameras
* Fix mirror lockup state for Canon 6D Mark II and other Canon cameras
* Fix mappings for ISO, Drive Mode, and White Balance camera properties on Canon cameras
* Fix use of MRAW and SRAW image formats on Canon cameras
* Fix possible photo save errors, if Filename Expression option gets set to empty text
* Fix mac support for generic PTP cameras
* Allow Photo Download Directory to be set from External API
* Add Microsoft .NET v4.0 package to windows installer
* Remove ability to send feedback from app

Version 3.37
------------------

21st May 2018
^^^^^^^^^^^^^^^^^^^^^^

* Fix possible app hang on windows, during handling of unexpected camera disconnection
* Fix possible crash on mac, if camera gets disconnected while downloads are pending 
* Improve scheduling of download threads for multi-camera parallel downloads
* Enable USB fast poll when camera battery is fully charged

Version 3.36
------------------

11th February 2018
^^^^^^^^^^^^^^^^^^^^^^

* Fix auto focus for Canon 750D and other newer Canon cameras
* Fix focus stacking script for some older Nikon Cameras
* Add filename expression token [X] for camera serial number

Version 3.35
------------------

24th January 2018
^^^^^^^^^^^^^^^^^^^^^^

* Fix error when trying to set camera group for multiple cameras
* Fix support for Nikon D3400 liveview
* Search default data path if camera database file is not found
* Add new 'Group' camera selection mode to External API
* Update External API docs for CameraSelection and PhotoSelection fields

Version 3.34
------------------

26th November 2017
^^^^^^^^^^^^^^^^^^^^^^

* Fix issue with default storage mode not being applied after camera reconnection
* Fix failure to launch Windows app, caused by missing MSVC DLL
* Play intermittent beep when lost camera connection is detected

Version 3.33
------------------

15th November 2017
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D850
* Add ability to set per-camera display rotation
* Fix ability to shoot with older Canon cameras such as the 450D and 40D
* Fix resync of batch number when triggering multiple cameras
* Fix incorrectly disabled Connect/Disconnect buttons in Camera Controls UI

Version 3.32
------------------

9th September 2017
^^^^^^^^^^^^^^^^^^^^^^

* Fix crash during camera disconnect after synchronising clocks
* Fix crash during exit if images are still being loaded

Version 3.31
------------------

27th August 2017
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 200D / Rebel SL2 / Kiss X8
* Add option to disable auto rotation of images
* Add UI to allow selection of GRID network interface/adapter
* Fix crash when camera is disconnected but photo save operation is still pending
* Fix hang when using some Nikon cameras in Mac
* Fix issue with some Nikon cameras disconnecting after failed capture
* Fix intermittent failures with 'MoveFocus' script command, used by focus stacking script
* Fix some minor issues with Nikon D7500 camera properties
* Fix issue where video size was reported incorrectly in the UI
* Fix issue with GRID network connection changes being ignored
* Fix deletion of photos from camera for simulated Disk storage mode (Nikon D3xxx cameras)
* Attempt to recover photo status after camera disconnect/reconnect
* Improve UI visibility for display options menu
* Ensure photo display fading only happens if next download is underway

Version 3.30
------------------

9th August 2017
^^^^^^^^^^^^^^^^^^^^^^

* Fix hang when download failure causes a camera disconnection
* Fix incorrect filename when reshooting photo
* Update to External API to ZeroMQ v4.2.2

Version 3.29
------------------

7th August 2017
^^^^^^^^^^^^^^^^^^^^^^

* Fix bulb mode for Canon 5D Mark III
* Fix issue with corrupt camera communication on Mac version
* Fix issue of excessive memory usage during video downloads
* Fix crash when enabling/disabling External API
* Add support for strftime() formatting with [T] filename token
* Add support for camera groups
* Update Camera Controls UI to support camera groups
* Ensure display window does not try to load/view video files
* Override Canon drive mode setting during bulb mode
* Support External API relay of custom messages between GRID nodes
* Support External API shutter button control with Canon cameras

Version 3.28
------------------

1st June 2017
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 77D / 9000D
* Add support for Canon 800D / Rebel T7i / Kiss X9i
* Improve performance of photo file saving
* Improve download performance for continuous shooting
* Fix liveview for some Canon cameras such as the 7D and 5D Mark II
* Attempt to save photo to backup location when photo download/save fails
* Increase limit on max GRID network transfers to 99
* Ensure backslashes are converted correctly for filenames on Mac
* Ensure restart prompt is not shown when closing Options/Preferences window

Version 3.27
------------------

1st May 2017
^^^^^^^^^^^^^^^^^^^^^^

* Switch to DevMate for licensing system
* Remove use of Canon EDSDK for camera communication

Version 3.26
------------------

15th March 2017
^^^^^^^^^^^^^^^^^^^^^^

* Improve performance with multi-camera asynchronous downloads
* Balance downloads across detected USB host controllers
* Add individual tracking of camera busy status
* Add automatic detection of physical camera USB attach/detach
* Recover USB connection state after camera plug/unplug
* Rename 'Refresh List' button to 'Detect'
* Remove JPEG previewer script
* Change UI labels for supported cameras option
* Fix option for resetting sequence/batch number after barcode scan

Version 3.25
------------------

31th January 2017
^^^^^^^^^^^^^^^^^^^^^^

* Add warning popup for unexpected camera disconnection
* Add prototype for Canon EDSDK replacement
* Add USB download rate to information shown in camera table
* Improve UI for Log window
* Improve UI for Camera Controls window
* Fix UI layout of Options window on high DPI displays
* Fix photo scale when 1:1 pixel display ratio is requested
* Fix intermittent crash after camera becomes disconnected
* Fix possible buffer overflow/crash during camera detection on windows
* Stop excessive camera communication traffic with Nikon D3xxx models
* Ensure restart prompt is only show once when changing options
* Ensure camera trigger requests are rejected when mirror lockup is enabled
* Disable shoot action in camera context menu when mirror lockup is enabled
* Cleanup and reduce logging during camera detection

Version 3.24
------------------

16th November 2016
^^^^^^^^^^^^^^^^^^^^^^

* Fix error causing early expiration of trial version
* Fix intermittent hang caused by race condition with excessive logging
* Fix intermittent hang during shutdown, due to race condition with log messages
* Add support for easy bug/crash reporting using Bugsplat
* Reduce External API traffic message to trace level
* Remove unnecessary log warnings about unmapped Canon properties
* Stop duplicate work when setting names during camera connection

Version 3.23
------------------

5th November 2016
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 5D Mark IV
* Add display option for always showing the most recent photo
* Optimise GRID photo saving when using shared NAS storage
* Update to Canon EDSDK v3.5

Version 3.22
------------------

12th September 2016
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D5
* Add support for Nikon D500
* Add JPEG only option for GRID network photo syncing
* Add button to force resynchronise of camera batch numbers
* Synchronise batch number when triggered multiple cameras
* Optimise file operations for computing photo hash when saving files
* Improve accuracy of liveview zoom region size/position
* Improve performance of downloads from multiple Nikon cameras
* Fix ability to change camera settings from the camera table
* Fix/avoid crash in Microsoft Windows runtime on certain CPUs

Version 3.21
------------------

17th April 2016
^^^^^^^^^^^^^^^^^^^^^^

* Fix crash with Canon cameras on Mac OS X 10.11 (El Capitan)
* Add support for Canon 80D
* Add support for Canon 1300D / Rebel T6 / Kiss X80
* Add support for Canon 1D X Mark II
* Fix positioning of liveview zoom with rotated display
* Only rotate displayed photos if orientation if not specified in photo file
* Update to Canon EDSDK v3.4

Version 3.20
------------------

3rd April 2016
^^^^^^^^^^^^^^^^^^^^^^

* Support utf-8 text
* Update code signing certificate for compatibility with latest Windows requirements
* Add filename expression token [E] for photo file extension
* Add warning message for when Graphics card does not support OpenGL

Version 3.19
------------------

9th February 2016
^^^^^^^^^^^^^^^^^^^^^^

* Fix error when saving photos to sub-directories of photo download directory
* Fix crash when reading JPEG image if file contains bad data
* Fix crash if script interpreter cannot be initialised
* Fix crash on mac due to missing liblzma library
* Keep history of previous log file contents (up to 8mbyte limit)

Version 3.18
------------------

23rd January 2016
^^^^^^^^^^^^^^^^^^^^^^

* Fix issue of data file corruption after crash
* Fix crash on mac when opening photo in external editor app
* Fix layout of mac DMG package contents

Version 3.17
------------------

20th December 2015
^^^^^^^^^^^^^^^^^^^^^^

* Fix crash when deleting and then taking photo with the same filename
* Fix crash when UI has to resize scroll bars
* Detect JPEG orientation from Nikon photos
* Ensure Canon storage mode is not forced to Disk on first connection

Version 3.16
------------------

1st November 2015
^^^^^^^^^^^^^^^^^^^^^^

* Detect orientation of JPEG images and auto-rotate display
* Add ability for applying presets to custom camera selection

Version 3.15
------------------

7th September 2015
^^^^^^^^^^^^^^^^^^^^^^

* Fix ability to resume Nikon liveview after camera trigger
* Ensure Nikon liveview AF area mode is set to 'Normal' to allow UI focus positioning

Version 3.14
------------------

30th August 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add 'JPEG' camera Storage mode to allow selectively only downloading JPEG photos
* Improve performance of External API message communication

Version 3.13
------------------

8th August 2015
^^^^^^^^^^^^^^^^^^^^^^

* Allow trigger during liveview for Nikon D3200 and D3300
* Add Auto Focus action to camera context menu UI
* Ensure External API reports cameras from all GRID nodes
* Ensure External API receives event messages from all GRID nodes
* Relax restriction on valid IP host addresses for GRID nodes

Version 3.12
------------------

13th July 2015
^^^^^^^^^^^^^^^^^^^^^^

* Allow editing of camera settings directly from camera table UI
* Fix inconsistent camera status and settings when running scripts
* Ensure camera power information is updated across GRID network
* Ensure node remove requests are set to all GRID nodes
* Improve method for GRID node synchronisation
* Gracefully handle GRID nodes that have clashing IP addresses
* Only warn for GRID node mismatch against connected nodes
* Only save GRID node addresses that were active or manually added

Version 3.11
------------------

21st June 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add DOF Preview functionality for supported Nikon cameras
* Fix problem with how GRID logic handles network unavailable conditions
* Fix mouse input handling for retina displays

Version 3.10
------------------

1st June 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D7200
* Add support for Canon 5DS and 5DS R
* Add support for Canon 750D / Rebel T6i / Kiss X8i
* Add support for Canon 760D / Rebel T6s / 8000D
* Add support for Canon 1D Mark III and 1Ds Mark III
* Add ability to manually add GRID node by IP address
* Improve reliability and performance of GRID networking
* Save GRID node adddresses in database file
* Allow quick view of photos when changing selection in Recent Photos window
* Fix problem with saving UI state of Filename Options window
* Fix memory leak during External API usage

Version 3.9
------------------

14th May 2015
^^^^^^^^^^^^^^^^^^^^^^

* Fix issue with Nikon liveview mode continuously activating mirror/shutter
* Add option to customise spacing between photos in display window
* Move UI for display options to display window toolbar

Version 3.8
------------------

6th May 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add support for camera Drive Mode setting
* Add support for customising the liveview refresh rate
* Add option to reset sequence/batch number when Unique Taq or Barcode is changed
* Add possible fix for Canon communication relibility issues on Windows
* Add UI for customising logging levels in Log window
* Add internal support for half pressing shutter release button
* Split UI for Camera Presets and Filename Settings into separate windows
* Improve UI for managing the display's background image
* Ensure Nikon Storage mode is set to Card when starting video recording
* Ensure Camera Controls UI is consistent with the current camera status
* Fix app hang on Windows after failed Nikon USB transfers
* Fix Storage mode settings for Nikon D200, D40, D60, D80
* Fix Image Quality settings for Nikon D200, D40, D60, D80
* Save and restore the enabled log levels with app options

Version 3.7
------------------

23rd March 2015
^^^^^^^^^^^^^^^^^^^^^^

* Allow re-ordering of UI table columns
* Fix error when loading decimal numbers for script UI
* Improve reliability of Canon camera management on Windows
* Reduce text width for UI table column names

Version 3.6
------------------

11th March 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D5500
* Add External API feature
* Add ability to select multiple cameras or photos and apply actions to them
* Add ability to hide photos from list, without deleting them
* Add Refresh List action to camera context menu
* Improve accuracy of camera clock syncing to (to +-100ms from local time)
* Improve detection of barcodes during automatic barcode scanning
* Improve reliability of camera detection on Mac
* Improve detection of duplicate Nikon cameras on Windows
* Extend actions available in photo context menu
* Ensure photo delete operations affect all copies across GRID network
* Fix issue with double filename extensions when using [O] in filename expression
* Fix UI layout issues with Options window
* Remove Reset Sequence/Batch number buttons from Options window
* Deprecate [N] token from filename expression

Version 3.5
------------------

1st February 2015
^^^^^^^^^^^^^^^^^^^^^^

* Fix identification and communication problems with Canon 100D / SL1
* Fix ability to stop script once it's already running
* Fix changing of camera settings across multiple cameras
* Fix missing DLLs in Windows 32-bit installer
* Update Mac OS X minimum version to 10.7 (Lion)
* Force camera Storage mode setting to Disk if no memory card is inserted
* Ensure formatting is only requested for valid memory cards
* Ensure Connect/Disconnect buttons reflect camera status
* Add camera information for number of memory cards inserted
* Add visible warning for GRID node version mismatch
* Stop printing of duplicate MTP camera device information

Version 3.4
------------------

20th January 2015
^^^^^^^^^^^^^^^^^^^^^^

* Fix batch number increments across grid network
* Fix crash during photo status updates
* Add batch number column to photo table
* Allow secondary display windows to go full screen

Version 3.3
------------------

18th January 2015
^^^^^^^^^^^^^^^^^^^^^^

* Add UI warning for camera batch number mismatches
* Add new EnableDOF/DisableDOF camera script commands
* Add Windows DLL for VC++ OMP redistributable
* Add UI button to force refresh of table information
* Improve behaviour of UI photo transfer progress bar
* Improve handling of Canon video recording state
* Improve network logic for managing/closing duplicate node connections
* Ensure accuracy of clock syncing is not affected by application latency
* Track photo sequence and batch number correctly for grid nodes with not connect cameras
* Fix bug where Nikon D3xxx photo downloads would save zero sized files
* Allow UI table columns to be hidden
* Ensure UI for camera and photo table resizes correctly to information
* Reduce noise of logging for camera messages

Version 3.2
------------------

3rd December 2014
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 7D Mark II

Version 3.1
------------------

23rd November 2014
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D750
* Add preliminary support for Nikon Coolpix cameras
* Add button to show/hide camera and photo names in the display window
* Add warning indicator for when multi-camera settings are mismatched
* Add option to set a default camera autofocus mode
* Add ability to enable/disable Mirror Lockup on Canon
* Add button for anticlockwise photo rotation
* Improve UI for adding camera presets
* Allow camera presets to include all available camera settings
* Support display autorotate for RAW photo orientation
* Ensure busy icon is shown whilst local camera operations are pending
* Convert semicolons to underscores in photo filenames
* Harmonise text used for camera autofocus modes
* Harmonise text used for Canon exposure compensation settings
* Show columns for camera settings in the camera table
* Add filename expression token [M] for milliseconds
* Allow filename expression [D] and [T] (date/time) to include a separator character
* Allow SetActiveCamera script function to take camera name as parameter

Version 3.0
------------------

2nd November 2014
^^^^^^^^^^^^^^^^^^^^^^

* Smart Shooter GRID - networked multi-computer control of multi-camera arrays
* Switch to 64 bit for Windows version
* Switch to 64 bit for Mac version
* Add support for Nikon D810
* Add support for Nikon D3300
* Add support for Live View on Nikon D3200 and D3300
* Add new UI for listing cameras and photos
* Add ability to scan barcodes directly from captured images
* Add ability to recompute photo filename after capture
* Add ability to save and load camera settings as presets
* Add ability to synchronise camera clocks
* Add ability to format camera memory cards
* Add ability to alter brightness of Liveview image
* Add Liveview only display mode
* Add camera and photo name labels to display window
* Add UI context menus to display window for camera/photo actions
* Improve and optimise performance of display drawing
* General improvements to UI appearance
* Set default Storage mode to Disk
* Ensure mouse wheel does not change dropdown menu selections
* Ensure camera database names are available before connecting to camera
* Support Retina display resolutions on Mac
* Harmonise names used for Canon and Nikon image quality settings
* Support TIFF and PNG background images
* Fix crash on some AMD Radeon HD graphics cards

Version 2.18
------------------

25th May 2014
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D4s
* Add support for changing focus mode on Nikon
* Add confirmation prompt for Options reset button
* Fix capture on Nikon D3x
* Fix problem with 'FULL PC' message on some Canon cameras
* Fix long running memory leak and crash for Nikon on Windows
* Fix for possible image download corruption for Nikon on Windows

Version 2.17
------------------

27th April 2014
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 1200D
* Improve Nikon photo download speed on Windows
* Fix crash on mac caused by old 'disable asynchronous camera communication' option
* Always show last known aperture/shutter speed for Canon

Version 2.16
------------------

10th April 2014
^^^^^^^^^^^^^^^^^^^^^^

* Fix long running memory leak and crash on Mac
* Fix crash/hang on Mac when communicating with cameras during reconnection
* Fix duplicate phantom Canon cameras listed on Mac
* Add option to disable loading of script during start up

Version 2.15
------------------

26th March 2014
^^^^^^^^^^^^^^^^^^^^^^

* Support for Nikon Df
* Support for Nikon D5300
* Support for bulb mode shooting on Nikon
* Fix crash during reconnection of Nikon cameras
* Fix long running increase in memory usage due to infinite log history
* Improve accuracy of time intervals in camera script loops
* Improve information about Nikon shutter speed setting
* Add delayed trigger script
* Add multicamera option to focus stacking script
* Add new WaitUntil camera script function

Version 2.14
------------------

4th December 2013
^^^^^^^^^^^^^^^^^^^^^^

* Support for Canon 70D
* Support for Nikon D610
* Fix problem with unusable camera settings when live view is enabled
* Ensure Nikon live view zoom is set to 1:1 pixel ratio
* Ensure live view is resumed after using TakePhoto script command
* Enable prompt for feedback on Mac app store
* Fix URL used for latest version check

Version 2.13
------------------

3rd November 2013
^^^^^^^^^^^^^^^^^^^^^^

* Ensure live view is resumed after taking a photo
* Fix bug where incorrect photo was displayed when selecting from Photos window
* Fix failure to close app when exiting with live view enabled
* Add script for delayed trigger of camera
* Add option to time lapse script to support multiple cameras

Version 2.12
------------------

12th September 2013
^^^^^^^^^^^^^^^^^^^^^^

* Major update to Nikon camera communication on Mac (for improved reliability)
* Support for Nikon D7100 (mac and windows version)
* Support for Nikon D3000, D3100, and D3200 (mac and windows version)
* Fix inconsistency of options presets after startup
* Fix issue with persistent storage of Application Name option
* Fix crash when using identify camera button in camera database
* Rename 'Customer Text' to 'Unique Tag' in UI

Version 2.11
------------------

5th August 2013
^^^^^^^^^^^^^^^^^^^^^^

* Support for Canon 100D and 700D
* Support for Nikon D7100 (windows version only)
* Add new camera setting for auto focus mode
* Add ability to reshoot a specific photo with the same filename
* Add ability to delete a specific photo
* Add presets for photo download/filename options
* Add filename expression variable for customer text
* Fix issue causing failure to start when asynchronous camera communication is disabled
* Fix issue causing failure to start when running over Windows remote desktop
* Fix crash on Windows when Nikon cameras are unexpectedly disconnected
* Fix problem with consistency of camera name database during camera connection
* Fix problem with consistency of camera name after edit
* Improve UI for Script Controls window
* Add slave camera script
* Allow manual memory card formatting on some Nikon models whilst tethered

Version 2.10
------------------

5th June 2013
^^^^^^^^^^^^^^^^^^^^^^

* Support for Nikon D3000, D3100, and D3200 (windows version only)
* Support multiple simultaneous Nikon cameras (windows version only)
* Support for Nikon video recording (windows verison only)
* Fix for Canon video recording
* New camera database for assigning custom names to each camera
* Fix batch numbering scheme for photo file names

Version 2.9
------------------

26th April 2013
^^^^^^^^^^^^^^^^^^^^^^

* Improve process for registering license information

Version 2.8
------------------

3rd April 2013
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Canon 6D, 5D Mark III, 1D C, 650D/Rebel T4i
* Add ability to control multiple cameras at once
* Add ability to start/stop video recording for Canon cameras
* Sort camera drop down list alphabetically
* Add display indicator for connection status of multiple cameras

Version 2.7
------------------

8th March 2013
^^^^^^^^^^^^^^^^^^^^^^

* Add preliminary support for Nikon D5200
* Add minimum width specifier for photo naming policy
* Add Z keyboard shortcut for multi shot
* Fix problem starting app on Mac OS X 10.5

Version 2.6
------------------

30th January 2013
^^^^^^^^^^^^^^^^^^^^^^

* Redesign and improve how photo filenames are generated
* Add ability to disable automatic photo display
* Add setting for default display mode
* Reduce memory usage of application by disabling image caching
* Fix failure to start on some Windows XP machines
* Use camera name stored in Canon cameras
* Add simple script for shutter speed bracket

Version 2.5
------------------

14th January 2013
^^^^^^^^^^^^^^^^^^^^^^

* Fix problem with blank live view display with Nikon D600
* Fix rare connectivity problem with some Nikons on windows
* Add option to store each photo in a sub directory separately for each connected camera

Version 2.4
------------------

19th December 2012
^^^^^^^^^^^^^^^^^^^^^^

* Add ability to set a background display image
* Add ability to upload log file to support server
* Add option for fixing compatibility with problematic Nikon cameras on Mac
* Fix display problem with Intel HD 3000 graphics cards, as used by some Macs

Version 2.3
------------------

20th November 2012
^^^^^^^^^^^^^^^^^^^^^^

* Add support for Nikon D600
* Add option for automatically connecting cameras
* Add option for setting a default Storage mode
* Add option for fade transition when waiting for new photo to load
* Fix problem when using optional script parameters

Version 2.2
------------------

12th September 2012
^^^^^^^^^^^^^^^^^^^^^^

* Fix for issues with Nikon D800 live view
* Fix for issue with Canon auto focus during live view
* Fix for error with focus stacking script
* Live view is turned back on after taking photo
* Live view scale and position are restored
* Easier exit from full screen mode

Version 2.1
------------------

13th August 2012
^^^^^^^^^^^^^^^^^^^^^^

* Add icon for windows uninstaller
* Ensure application adjusts layout to fit screen size
* Try and set Canon disk space to avoid PC FULL message on camera
* Ensure update window shows the correct current version

Version 2.0
------------------

6th August 2012
^^^^^^^^^^^^^^^^^^^^^^

* Application has been redesigned to be more user friendly, with dark styling to minimise distraction from the photos themselves
* Graphics system has been rewritten to improve performance and add support for multiple windows and multiple monitors
* Script interface has been updated so that its more intuitive to load scripts and control script parameters, and includes built-in scripts for timelapse, HDR, HDR timelapse, and focus stacking
* Improvements to reliability of camera communication, with faster photo download and preview
* Support for Nikon D4, D800, and D800E
* Support for Canon 650D, 5D Mark III, and 1D X (windows version only).

Version 1.1.14
------------------

14th February 2012
^^^^^^^^^^^^^^^^^^^^^^

* Updated to latest Nikon SDK

Version 1.1.13
------------------

8th February 2012
^^^^^^^^^^^^^^^^^^^^^^

* Fixed issue with bulb mode shooting on some recent Canon cameras

Version 1.1.12
------------------

2nd February 2012
^^^^^^^^^^^^^^^^^^^^^^

* Fixed issue with Nikon camera connection reliability on mac
* Fixed issue with Canon autofocus when shooting with live view

Version 1.1.11
------------------

3rd September 2011
^^^^^^^^^^^^^^^^^^^^^^

* Fixed issue with Live View zoom on Nikon
* Allow application to start again if it has previously not closed correctly

Version 1.1.10
------------------

3rd September 2011
^^^^^^^^^^^^^^^^^^^^^^

* Fixed issue with JPEG decoder
* Fixed issue with Nikon D40x

Version 1.1.9
------------------

29th August 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added auto focus support for most Canon cameras
* Improved performance of live view mode

Version 1.1.8
------------------

31st July 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added shortcut keys for some of the camera settings
* Added logic to check only one instance of the application is running
* Added full screen mode
* Fixed crash when using buggy OpenGL drivers (naughty Intel 965/963 Graphics Media Accelerator!)

Version 1.1.7
------------------

23rd June 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Nikon D5100
* Fixed problem with bulb shooting on Canon 600D/Rebel T3i and Canon 1D Mark IV
* Fixed issue parsing TCL script parameters that contain certain characters, such as ':'
* Added example script for timelapse shooting with bulb exposures

Version 1.1.6
------------------

14th May 2011
^^^^^^^^^^^^^^^^^^^^^^

* Fixed issue causing failure to start with some OpenGL drivers
* Fixed issue for Nikon camera communication that caused failure after 100 photos
* Added 15 use limit for trial version

Version 1.1.5
------------------

14th April 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Canon 600D (Rebel T3i) and Canon 1100D (Rebel T3)
* Reduced since of Mac version by stripping out redundant code
* Fixed rare crash on Mac related to loading rogue Qt plugins

Version 1.1.4
------------------

4th April 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added option to wake screensaver when a new photo is taken
* Fixed bug that caused crash if the return result of a script was too long
* Added TCL example script for delayed shooting timer

Version 1.1.3
------------------

15th March 2011
^^^^^^^^^^^^^^^^^^^^^^

* Improved reliability of Canon photo download
* Added new TCL functions &#8216;GetPhotoDetails&#8217; and &#8216;GetCameraDetails&#8217;
* Added log output for TCL function return values
* Disabled bulb shooting button for Nikon
* Fixed bug with photo filenames for Nikon
* Added TCL example script with SmartShooter stub functions

Version 1.1.2
------------------

22nd February 2011
^^^^^^^^^^^^^^^^^^^^^^

* Added new icon and title graphics
* Improved reliability of &#8216;Reset Layout&#8217; menu option
* Updated Nikon camera modules to latest version

Version 1.1.1
------------------

8th February 2011
^^^^^^^^^^^^^^^^^^^^^^

* Fixed compatability with OpenGL v1.1 and above
* Added ability to specify photo filename sequence number
* Added support for minidumps for crash analysis in Windows

Version 1.1.0
------------------

31st December 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Nikon D40, D60, and D80
* Added ability to save GUI layout for next time program is run
* Ensured GUI layout adapts to small screen sizes (such as Macbook Air 11&#8243;)
* Fixed minor issues with GUI rendering on Mac
* Added HDR timelapse example scriptsFixed problem with TCL script interpreter

Version 1.0.702
------------------

26th December 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Nikon D7000

Version 1.0.690
------------------

20th December 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added ability to rotate photo display
* Added preliminary support for Nikon D200
* Standardised names for aperture and shutter speed values between Nikon and Canon
* Improved logging information to help when writing scripts

Version 1.0.598
------------------

11th December 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added customisable photo filename prefix for Nikon
* Added &#8216;MultiShot&#8217; ability to take a photo with all connected cameras at once
* Fixed live view display when multiple cameras have live view enabled
* Fixed aperture controls when using Canon 1D Mark IV in Bulb shooting mode

Version 1.0.588
------------------

24th November 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed compatability with Mac OS X 10.4 (Tiger) and 10.5 (Leopard)

Version 1.0.582
------------------

19th November 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added Auto Focus functionality (Nikon only at the moment)
* Set log viewer window to float by default
* Fixed issue that caused the view 1&#215;1 button to scale the photo incorrectly
* Fixed crash when a photo download attempt fails

Version 1.0.560
------------------

10th November 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved reliability of script commands
* Fixed issue with Nikon live view stopping after photo is taken
* Added support for Nikon D3x

Version 1.0.546
------------------

6th November 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed crash on certain graphics hardware when loading large photos
* Fixed issue causing error dialog to be shown when trial is exited
* Added optional Microsoft Visual C++ 2005 runtime installer (required by Nikon image library)

Version 1.0.538
------------------

3rd November 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved performance of Nikon live view
* Added Depth of Field (DOF) preview option for Canon live view
* Set log file size limit to 10MB
* Updated install package so example scripts are more obvious
* Added missing DLL for Nikon NEF library
* Fixed problem with Canon unknown image quality settings on Mac OS X

Version 1.0.525
------------------

30th October 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed crash with graphics cards that do not support OpenGL v2

Version 1.0.522
------------------

28th October 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Nikon D300 and D300s
* Added support for Canon D60
* Added error reporting dialog in case application closes unexpectedly

Version 1.0.512
------------------

22th October 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Nikon D90 and Nikon D5000
* Added indicator for camera battery status
* Added context menu to launch external file browser for photo file
* Fixed issue with image histogram: blue and green channels were incorrectly swapped
* Reduced trial photo limit to 30 photos

Version 1.0.483
------------------

6th October 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added histogram display for the photo colour channels
* Added option to highlight areas of the photo where the colour is at the edge of the colour range
* Added option to display a grid line overlay
* Added option to auto run the current script whenever a new photo is taken
* Added option to open photo in an external editor application
* Added context menu to the Photo List window
* Added command line parameter to specify a script to run immediately after start up: SmartShooter.exe -r myscript.tcl
* Improved performance when synchronising camera settings

Version 1.0.462
------------------

22th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added ability to take photos with bulb exposure timer
* Added tool tips to user interface

Version 1.0.451
------------------

20th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added ability to record each Live View frame as a JPEG image
* Added ability to download all photos
* Added option to set the background colour
* Fixed crash on Mac when validated license

Version 1.0.440
------------------

19th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added mouse cursor icons to photo display
* Added button to view photo at 1:1 pixel to screen ratio
* Improved Photo List interface

Version 1.0.432
------------------

16th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed problem when loading a new script

Version 1.0.426
------------------

15th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added ability to change White Balance and Metering Mode
* Fixed problem with ISO range control when using Canon 5D Mark II
* Fixed notification message when a new camera is detected

Version 1.0.417
------------------

12th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved performance of Live View
* Mac version: Fixed crash whilst processing RAW photos

Version 1.0.412
------------------

10th September 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved reliability of Live View
* Added user defined parameters to camera script controls

Version 1.0.377
------------------

24th August 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed crash in JPEG loader

Version 1.0.371
------------------

17th August 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved performance of JPEG processing
* Added functionality for registering a license
* Removed beta license expiry

Version 1.0.350
------------------

18th May 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added support for Mac OS X (Intel)

Version 1.0.341
------------------

17th May 2010
^^^^^^^^^^^^^^^^^^^^^^

* Improved GUI interface
* Added ability to interrupt and stop a running script

Version 1.0.290
------------------

30th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed possible problem on some Canon EOS cameras with Live View operation
* Fixed bug which caused some values for Exposure Compensation setting to be missing
* Improved performance of log window

Version 1.0.284
------------------

29th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added ability to change position of live view zoom
* Added keyboard shortcuts for moving and zooming the display

Version 1.0.278
------------------

24th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added Exposure Compensation setting
* Fixed handling of camera disconnection
* Fixed crash during manual photo download

Version 1.0.272
------------------

18th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Updated trial expiry date

Version 1.0.268
------------------

17th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Correctly positioned live view output when zoom is enabled
* Added log viewer for diagnostics

Version 1.0.253
------------------

7th March 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added option for viewing full RAW image
* Fixed crash when camera settings change

Version 1.0.219
------------------

13th February 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added controls for live view focus
* Simplified interface layout

Version 1.0.214
------------------

11th February 2010
^^^^^^^^^^^^^^^^^^^^^^

* Added Aperture setting
* Added Shutter Speed setting
* Added ISO setting
* Added Image Quality setting

Version 1.0.201
------------------

7th February 2010
^^^^^^^^^^^^^^^^^^^^^^

* Changed RAW photo display to use preview JPEG data
* Improved photo download speed
* Saved options to hard disk
* Converted interface to docking layout

Version 1.0.140
------------------

11th January 2010
^^^^^^^^^^^^^^^^^^^^^^

* Fixed option to set photo download directory

Version 1.0.134
------------------

10th January 2010
^^^^^^^^^^^^^^^^^^^^^^

* Disabled TEST camera
* Set log directory to Windows application data folder
* Made interface layout DPI aware

Version 1.0.121
------------------

6th January 2010
^^^^^^^^^^^^^^^^^^^^^^

* First release!
