License Activation
==================
 
*How to activate your license for Smart Shooter*

After purchasing a license for Smart Shooter, you will receive an email
containing the license information. This email is sent to the same email
address that you specified when making the purchase.

The license email contains the activation code in the text of the email. The
screenshot below shows an example of the license email, with the activation
code text highlighted in yellow.

.. figure:: images/devmate_email.png

Activating the license
----------------------

When Smart Shooter is started, it will show the following window. Click the
**Enter Activation Number** button.

.. figure:: images/devmate_trial.png

The **Activation** window, as shown below, is where you can enter your
activation code. So, copy the code from your email, and then paste it into the
box here.

.. figure:: images/devmate_activate.png

Once the license has been checked and verified, you will see the confirmation
as shown below.

.. figure:: images/devmate_success.png

If you ever need to deactivate the license, you can click the menu item as
shown below. You may need to do this if you want to install the software
on another computer and use the same license there. 

.. figure:: images/devmate_deactivate.png
