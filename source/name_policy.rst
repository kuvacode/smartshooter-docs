Name Policy
===========

*How to customise for filename when saving photos*

Whenever Smart Shooter detects that a new photo has been taken, it will assign
that photo a unique filename. The actual filename to use is computed
dynamically by the app, and is controlled by the current name policy. These
options can be customised in the **Options** window under the **Name Policy**
tab, as shown in the screenshot below.

.. figure:: images/name_policy.png

Filename Expression
-------------------

The filename expression is the text that specifies how each new filename is
computed. This text may contain special identifiers, as letters inside a pair
of square brackets. These parts are automatically extracted and replaced with
dynamic information, by following some rules for each letter.

For example, if the text contains ``[D]`` then this part of the filename will
be replaced with the current date. The full set of rules is shown in the table
below.

+--------+----------------------+---------------------------------------------+
| Letter | Meaning              | Description                                 |
+========+======================+=============================================+
| **N**  | Session name         | A bit of text, that can be changed easily   |
|        |                      | in the app's UI.                            |
+--------+----------------------+---------------------------------------------+
| **I**  | Session number       | A number that can be manually incremented   |
|        |                      | and decremented by clicking a button in the |
|        |                      | app's UI.                                   |
+--------+----------------------+---------------------------------------------+
| **S**  | Sequence number      | A number that is automatically incremented  |
|        |                      | after each new photo is taken. Using this   |
|        |                      | ensures that each photo has a unique        |
|        |                      | filename.                                   |
+--------+----------------------+---------------------------------------------+
| **B**  | Batch number         | A number that is associated with each       |
|        |                      | camera, and automatically incremented       |
|        |                      | whenever that camera takes a new photo. So  |
|        |                      | effectively this is a per-camera sequence   |
|        |                      | number. When the camera is set to save each |
|        |                      | photo to RAW + JPEG files with a dual       |
|        |                      | format quality setting, this number will    |
|        |                      | increment just once per exposure, so the    |
|        |                      | RAW and JPEG files will use the same        |
|        |                      | number.                                     |
+--------+----------------------+---------------------------------------------+
| **C**  | Camera name          | Name of the camera that took the photo.     |
+--------+----------------------+---------------------------------------------+
| **X**  | Camera serial number | Serial number of the camera that took the   |
|        |                      | photo.                                      |
+--------+----------------------+---------------------------------------------+
| **O**  | Original photo name  | If the camera itself generated a filename,  |
|        |                      | this contains it.                           |
+--------+----------------------+---------------------------------------------+
| **E**  | Photo file extension | The file extension of the photo             |
|        |                      | (for example JPG, NEF, CR2 etc).            |
+--------+----------------------+---------------------------------------------+
| **U**  | Unique tag           | A unique bit of text, that can be changed   |
|        |                      | easily in the app's UI.                     |
+--------+----------------------+---------------------------------------------+
| **Z**  | Barcode              | The text from the last barcode that was     |
|        |                      | scanned by the app.                         |
+--------+----------------------+---------------------------------------------+
| **A**  | App name             | The name associated to the local app, which |
|        |                      | can be changed in the Options window.       |
+--------+----------------------+---------------------------------------------+
| **D**  | Date                 | The current date in the format YYYYMMDD.    |
+--------+----------------------+---------------------------------------------+
| **T**  | Time                 | The current time in the format HHMMSS.      |
+--------+----------------------+---------------------------------------------+
| **M**  | Milliseconds         | The millisecond part of the current time as |
|        |                      | a number between 000 and 999.               |
+--------+----------------------+---------------------------------------------+
| **G**  | Camera Group         | The name of the group that the camera       |
|        |                      | belongs to.                                 |
+--------+----------------------+---------------------------------------------+

.. note::
   If the filename computed is not unique, it may clash with an existing photo
   file that has already been saved. If this happens, Smart Shooter has a safety
   mechansim to ensure no files are overwritten, see :ref:`Overwriting Files`.
   So if the final filename has an unexpected ``_1`` or ``_2`` suffix, this is the reason.

Quickly Changing Filename Options
---------------------------------

The Smart Shooter UI has a decidicated **Filename Options** window where this
information can be quickly checked and edited, as shown in the screenshot
below.

This allows you to change the ``[N]`` Session Name text and ``[U]`` Unique Tag
text, manually increment and decrement the ``[I]`` Session number, and manually
reset the ``[S]`` Sequence number and ``[B]`` Batch number. These changes take
effect immediately and will be reflected in the filename for any subsequent
photos that are taken.

.. figure:: images/filename_options_window.png

This window also shows the current **Barcode** information. Smart Shooter has
the ability to scan photos for barcodes or QR codes, and extract the text from
them. Such barcode text can then be used as part of the filename by using
``[Z]``. For more information on how the barcode scanning works, see
:ref:`Barcode Scanning`.

Using Subdirectories
--------------------

The filename expression can also include the directory separator ``/``. This
allows one or more subdirectories to be included in the final filename. The
final filename will always be relative to the current
**Photo Download Directory**, which can be set in the **Options** window.

Using Camera Generated Name
---------------------------

Although the ``[O]`` Original photo name should contain the filename that was
generated by the camera, it only does this when using the ``Card`` Storage mode.
If the Storage mode is set to ``Disk`` or ``Both``, then the camera will not
provide information about the filename it generated, and hence Smart Shooter
will end up using text such as ``IMG_0000`` instead.

Setting Camera Names
--------------------

The ``[C]`` camera name is extremely useful when using multiple cameras, as
this allows the filename of each photo to contain the name of the camera that
took the photo. For each camera that is connected to Smart Shooter, the app
will store a name for the camera. This name is associated with the camera's
serial number. The name can be customised in the camera table UI, as shown
below:

.. figure:: images/rename_camera.png

Formatting Options
------------------

Each letter in square brackets can be followed by an optional number, which
will then control the minimum width of the replacement text. For numbers, the
number will be padded with extra leading ``0``'s. For text, it will be padded
with extra leading ``_``'s (underscore).

For example, if the current sequence number is 23, then ``[S]`` will be
replaced with ``23``, whereas ``[S4]`` will be replaced with ``0023``.

The ``[D]`` and ``[T]`` (date and time) letters can also take an extra
character, which is then used as a separator between the date or time
components. For example, if the current date is 20th August 2011, then
``[D]`` will be replaced by ``20110820``, whereas ``[D-]`` will be
replaced by ``2011-08-20``.

Extended formatting for date/time
---------------------------------
The ``[T]`` letter also has an extended option, to allow extra customisation
of how the date/time is generated. After the ``T`` character you can specify
an arbitrary string, which is then passed to the standard strftime() function
for formatting. For example ``[T%A %d %B %Y]`` will pass the string
``%A %d %b %Y`` to strftime(), which will format it as
``Saturday 20 August 2017``.

For more information on what can be handled by strftime():
`<http://www.cplusplus.com/reference/ctime/strftime/>`_.

Invalid Characters
------------------
Any invalid characters in the computed filename will be replaced with an
``_`` (underscore) character. This includes the space character and the
``:`` (semicolon) character.

Examples
--------

The following table shows some examples of the filename expression, and what
the resulting computed filename would be. This assumes the photo taken is a
JPEG, that the current sequence number is 128, the current batch number is 23,
the camera that took the photo is named "centre", The current unique tag is
"modelX", and that the current date is 20th August 2011.

+---------------------+-------------------------------+
| Filename Expression | Computed filename             |
+=====================+===============================+
| SSP_[S4]            | SSP_0128.jpg                  |
+---------------------+-------------------------------+
| [D]/[U]_[S4]        | 20110820/modelX_0128.jpg      |
+---------------------+-------------------------------+
| [D]_[U]/[C]_[B]     | 20110820_modelX/centre_23.jpg |
+---------------------+-------------------------------+

Overwriting Files
-----------------

Smart Shooter has a safety mechanism so that it will never overwrite an
existing file. This guards against the accidental loss of photos. Whenever the
app is about to save a photo, it first checks whether there is an existing file
with the same filename. If it finds an existing file, then it will append a
``_1`` to the end of the filename and then try again. If that new filename also
already exists, then it will append a ``_2``, and try again, and continue to
try with incrementing number suffixes.
