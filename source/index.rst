.. smartshooter documentation master file, created by
   sphinx-quickstart on Fri Jan 11 11:25:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive. 

.. figure:: images/smartshooter_logo.png

Smart Shooter Documentation
===========================


.. toctree::
   :maxdepth: 1

   installation
   activation
   user_guide
   name_policy


Smart Shooter Pro Features:
---------------------------

.. toctree::
   :maxdepth: 1

   barcode_scanning
   external_api
   custom_scripting
   multicamera
   lightroom_plug-in

Release Notes:

.. toctree::
   :maxdepth: 1

   release_notes