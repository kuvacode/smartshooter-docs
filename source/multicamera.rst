Multi-Camera
============

.. note::
   This feature is only available in **PRO** versions of Smart Shooter. 

Connect and Control Multiple Cameras
------------------------------------

While the standard version of Smart Shooter will allow up to three cameras to be connected, you can only shoot with one camera at a time.

Smart Shooter Pro allows you to connect up to 8 cameras to you PC or Mac at the same time. The interface allows you to select which camera is currently being controlled and hence will be affected by changes to settings and other commands.

You can find out which cameras are currently connected by clicking on the Cameras tab at the top of Smart Shooter.

.. figure:: images/multi_camera.jpg

To see the status of each camera, look in the column labeled "Status"

How many cameras can be connected to Smart Shooter?
---------------------------------------------------

Smart Shooter 4 Pro allows up to 8 cameras to be connected and used simultaneous. The computer's USB system is the limiting factor for this, as each additional camera will use 1 USB port, extra bandwidth and require extra CPU processing to maintain the connection.

When connecting multiple cameras, try to use good quality powered USB hubs and `TetherPro USB Cables <https://www.tethertools.com/product-category/cables-adapters/>`_. Also experiment with connecting to different USB ports on the computer itself as these can vary in performance. If the computer's own USB ports are supected to be the limiting factor, then try installing a dedicated USB PCIExpress card into the computer.

If you are unable to connect all the cameras to a single computer, then you need to use multiple computers and split the cameras between then. Smart Shooter GRID can then be used to coordinate and control the cameras across multiple computers.
