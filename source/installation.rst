Installation Guide
======================

**Windows** 
^^^^^^^^^^^^^

*Minimum Requirements*

- OS Windows Vista (64bit) or later
- 100MB free disk space

- To install, download the file from the website, open it, and follow the on-screen instructions.
- To uninstall Smart Shooter, run the Uninstall shortcut in the application folder.
- Or select Add/Remove Programs in the Windows Control Panel to Remove Smart Shooter.

**Mac**
^^^^^^^^^^^^^

*Minimum Requirements*

- Mac OS X 10.12 (Sierra) or later
- 100MB free disk space

- Smart Shooter is provided as a DMG package file.
- To install, download the file from the website, open it, and drag the SmartShooter file to your Applications folder. 
- To uninstall Smart shooter, delete the Smart Shooter file in the Applications folder. `

**How to Update**
^^^^^^^^^^^^^^^^^^
New releases of Smart Shooter will be made available on the download section of the `website <https://www.tethertools.com/product/smart-shooter-4/#specs>`_. You can install a new version without uninstalling the previous version, and your personal settings will also be preserved.