#!/usr/bin/env python
#
# Copyright (c) 2015, Kuvacode Oy
# All rights reserved. No warranty, explicit or implicit, provided.
#

import zmq

def main():
    seq_num = 0
    context = zmq.Context()
    sub_address = "tcp://127.0.0.1:54543"
    sub_socket = context.socket(zmq.SUB)
    sub_socket.setsockopt(zmq.SUBSCRIBE, b"")
    sub_socket.connect(sub_address)
    print("Opened listener to: {0}".format(sub_address))
    while (True):
        raw = sub_socket.recv()
        json_msg = raw.decode("utf-8")
        print("Received: {0}".format(json_msg))

if __name__ == '__main__':
    main()
