Photo Download
==============

*How to control and automate downloading photos*

When a new photo is taken, the actual location of the photo file is determined
by the **Storage** camera setting. This provides control over where the photo
is initially saved, and whether it is automatically downloaded. This setting
has four possible values:

+----------+-----------------------------------------------------------------+
| Storage  | Description                                                     |
+==========+=================================================================+
| **Disk** | The photo is not saved to the camera's memory card, but instead |
|          | is immediately downloaded and saved to the local computer disk. |
+----------+-----------------------------------------------------------------+
| **Card** | The photo is saved to the camera's memory card, and not         |
|          | automatically downloaded.                                       |
+----------+-----------------------------------------------------------------+
| **Both** | The photo is saved to the camera's memory card, and also        |
|          | immediately downloaded and saved to the local computer disk.    |
+----------+-----------------------------------------------------------------+
| **JPEG** | The photo is saved to the camera's memory card. If the photo is |
|          | in JPEG format then it is also immediately downloaded and saved |
|          | to the local computer disk.                                     |
+----------+-----------------------------------------------------------------+

Once the photo is downloaded, if the camera is active in the display window,
then the photo will be automatically viewed in the display window.

The **Storage** camera setting can be changed in the **Camera Controls**
window as shown below.

.. figure:: images/storage_mode.png

Manual Download
---------------

For each new photo that is taken, Smart Shooter will automatically detect this
and query the camera for information about this photo. The app will create a
new *Photo* entry, which will then be visible in the **Recent Photos**
window and also in the table of photos.

For photos that have not been downloaded yet and are currently on the camera's
memory card, they are a few ways to begin the download. The screenshot below
shows the table of photos. Above the table is the **Download All** button,
which will begin download of all such photos.

The screenshot also shows the context menu when a photo is right-clicked.
Selecting the **Download** action from this menu will then download the
currently selected photo.

.. figure:: images/photo_download.png

The table has a column labelled **Location**. This shows the current location
of each photo. If the photo has not yet been downloaded then the location will
be ``Camera``. Once a photo has been downloaded and saved to the local
computer disk, then the location will be ``Local Disk``.

Shooting RAW + JPEG
-------------------

When shooting in dual RAW + JPEG mode, the camera will save each photo as both
a RAW file and a JPEG file. The ``JPEG`` Storage option is particularly useful
in this scenario. RAW files will stay on the camera's memory card, but JPEG
files will be automatically downloaded. As JPEG files are much smaller than RAW
files, this allows an extemely quick visual preview of the photo.

The RAW photo and JPEG photo will be assigned a unique Sequence number in Smart
Shooter. However, they will both be assigned to the same Batch number. So
depending on the how the options are configured for filenaming, this may lead
to the RAW and JPEG photos be given different filenames. For more information
on how this numbering is used for photo filenames, see :ref:`Name Policy`.

Setting a default
-----------------

When a camera is first connected to Smart Shooter, the **Storage** mode will
be set to a specific value. This defaults to ``Disk``, but there is an option
to customise this. Go to the **Options** window and click the **General**
tab. Here in the **Default Settings** section there is a drop down box
labelled **Camera Storage**: this allows you to set the default Storage mode.

.. figure:: images/default_storage_mode.png