if [ ! -d "venv" ]; then
    python3 -m venv venv
fi
source venv/bin/activate
pip install wheel
pip install sphinx
pip install sphinx-rtd-theme
pip install sphinxcontrib-blockdiag
